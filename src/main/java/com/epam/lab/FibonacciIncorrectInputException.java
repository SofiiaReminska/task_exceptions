package com.epam.lab;

public class FibonacciIncorrectInputException extends RuntimeException {
    public FibonacciIncorrectInputException(){
        super();
    }
    public FibonacciIncorrectInputException(String msg){
        super(msg);
    }
}
