package com.epam.lab;

public class MyCloseableImpl implements AutoCloseable {
    @Override
    public void close(){
        System.out.println("I'll close in a moment...");
    }
}
