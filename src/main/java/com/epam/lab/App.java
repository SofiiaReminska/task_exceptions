package com.epam.lab;

import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * Fibonacci task!
 *
 * @author Sofiia Reminska
 */
public final class App {

    /**
     * Private constructor.
     */
    private App() {
    }

    /**
     * Entry point for intervals and fibonacci calculation program.
     *
     * @param args - input params for program
     */
    public static void main(final String[] args) throws Exception {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter range of numbers:");
        int leftRange = sc.nextInt();
        int rightRange = sc.nextInt();
        if (leftRange > rightRange) {
            throw new FibonacciIllegalSetSizeException("Incorrect input!!!");
        }
        printEvenNumbers(leftRange, rightRange);
        printOddNumbers(leftRange, rightRange);
        System.out.printf("Sum of even numbers is: %d%n",
                sumEvenNumbers(leftRange, rightRange));
        System.out.printf("Sum of odd numbers is: %d%n",
                sumOddNumbers(leftRange, rightRange));
        System.out.println("Enter size of Fibonacci set: ");

        int sizeOfFibonacciSet;
        try {
            sizeOfFibonacciSet = sc.nextInt();
        } catch (InputMismatchException e) {
            throw new FibonacciIllegalSetSizeException("Too big number set!!!");
        }
        printFibonacciStats(sizeOfFibonacciSet);
        demonstrateAutoCloseableClass();
    }

    /**
     * Method for printing even numbers from interval in reverse order.
     *
     * @param leftRange  - left range of interval
     * @param rightRange - right range of interval
     */
    public static void printEvenNumbers(final int leftRange,
                                        final int rightRange) {
        int rightRangeNumber = rightRange;
        System.out.printf("Even numbers from range: [%d,%d]%n",
                leftRange, rightRange);
        if (rightRangeNumber % 2 != 0) {
            rightRangeNumber -= 1;
        }
        while (rightRangeNumber >= leftRange) {
            System.out.printf("%d ", rightRange);
            rightRangeNumber -= 2;
        }
        System.out.println();
    }

    /**
     * Method for printing odd numbers from interval.
     *
     * @param leftRange  - left range of interval
     * @param rightRange - right range of interval
     */
    public static void printOddNumbers(final int leftRange,
                                       final int rightRange) {
        int leftRangeNumber = leftRange;
        int rightRangeNumber = rightRange;
        System.out.printf("Odd numbers from range: [%d,%d]%n",
                leftRangeNumber, rightRangeNumber);
        if (leftRangeNumber % 2 == 0) {
            rightRangeNumber += 1;
        }
        while (leftRangeNumber <= rightRangeNumber) {
            System.out.printf("%d ", leftRange);
            leftRangeNumber += 2;
        }
        System.out.println();
    }

    /**
     * Method for summation even numbers from interval.
     *
     * @param leftRange  - left range of interval
     * @param rightRange - right range of interval
     * @return - sum of even numbers from interval
     */
    public static int sumEvenNumbers(final int leftRange,
                                     final int rightRange) {
        int rightRangeNumber = rightRange;
        int sum = 0;
        if (rightRangeNumber % 2 != 0) {
            rightRangeNumber -= 1;
        }
        while (rightRangeNumber >= leftRange) {
            sum += rightRangeNumber;
            rightRangeNumber -= 2;
        }
        return sum;
    }

    /**
     * Method for summation odd numbers from interval.
     *
     * @param leftRange  - left range of interval
     * @param rightRange - right range of interval
     * @return - sum of odd numbers from interval
     */
    public static int sumOddNumbers(final int leftRange,
                                    final int rightRange) {
        int leftRangeNumber = leftRange;
        int rightRangeNumber = rightRange;
        int sum = 0;
        if (leftRangeNumber % 2 == 0) {
            rightRangeNumber += 1;
        }
        while (leftRangeNumber <= rightRangeNumber) {
            sum += leftRange;
            leftRangeNumber += 2;
        }
        return sum;
    }

    /**
     * Method for printing Fibonacci statistics.
     *
     * @param sizeSet - size of Fibonacci set
     */
    public static void printFibonacciStats(final int sizeSet) {
        final int percent = 100;
        int fib1 = 0;
        int fib2 = 1;
        int fib3;
        int countEvenFibonacciNumber = 1;
        int countOddFibonacciNumber = 1;
        System.out.printf("%d %d", fib1, fib2);
        for (int i = 2; i < sizeSet; i++) {
            fib3 = fib1 + fib2;
            if (fib3 % 2 == 0) {
                countEvenFibonacciNumber++;
            } else {
                countOddFibonacciNumber++;
            }
            System.out.print(" " + fib3);
            fib1 = fib2;
            fib2 = fib3;
        }
        System.out.println();
        System.out.printf("Even numbers: %s%%%n",
                (double) countEvenFibonacciNumber / sizeSet * percent);
        System.out.printf("Odd numbers: %s%%%n",
                (double) countOddFibonacciNumber / sizeSet * percent);
    }

    /**
     * Method for demonstrating the custom autocloseable class.
     */
    public static void demonstrateAutoCloseableClass() {
        try (MyCloseableImpl impl = new MyCloseableImpl()) {
            System.out.println("Now it will demonstrate how autocloseable class works!");
        } catch (Exception e) {
            System.out.println("Caught Exception!");
        }
    }
}
