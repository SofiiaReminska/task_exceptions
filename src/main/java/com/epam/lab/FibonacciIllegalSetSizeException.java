package com.epam.lab;

public class FibonacciIllegalSetSizeException extends IllegalArgumentException {
    public FibonacciIllegalSetSizeException(){
        super();
    }
    public FibonacciIllegalSetSizeException(String msg){
        super(msg);
    }
}
